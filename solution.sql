
-- create database
CREATE DATABASE blog_db;

-- create user table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	email VARCHAR(300) NOT NULL,
	password VARCHAR(300),
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);


-- create post table
CREATE TABLE posts(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	title VARCHAR(500) NOT NULL, 
	content VARCHAR(5000),
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_user_id
    	FOREIGN KEY (user_id) REFERENCES users (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);


-- create post comment table
CREATE TABLE post_comments(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	content VARCHAR(5000) NOT NULL,
	datetime_commented DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_comments_post_id
    	FOREIGN KEY (post_id) REFERENCES posts (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_post_comments_user_id
    	FOREIGN KEY (user_id) REFERENCES users (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);


-- create post likes table
CREATE TABLE post_likes(
	id INT NOT NULL AUTO_INCREMENT,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_likes_post_id
    	FOREIGN KEY (post_id) REFERENCES posts (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT,
    CONSTRAINT fk_post_likes_user_id
    	FOREIGN KEY (user_id) REFERENCES users (id)
    	ON UPDATE CASCADE
    	ON DELETE RESTRICT
);

-- insert data into user's table
INSERT INTO users(email, password, datetime_created) VALUES ("johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");

INSERT INTO users(email, password, datetime_created) VALUES ("johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");

-- inserting data into post's table
INSERT INTO posts(title, content, datetime_posted, user_id) VALUES ("First Code", "Hello Word", "2021-01-02 01:00:00" , 1);

INSERT INTO posts(title, content, datetime_posted, user_id) VALUES ("Second Code", "Hello Earth", "2021-01-02 02:00:00" , 1);

INSERT INTO posts(title, content, datetime_posted, user_id) VALUES ("Third Code", "Welcome to Mars", "2021-01-02 03:00:00" , 2);

INSERT INTO posts(title, content, datetime_posted, user_id) VALUES ("Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00" , 4);


-- Get all the title with a user ID of 1
SELECT title FROM posts WHERE user_id = 1;


-- Get all the user's email and date time of creation

SELECT email, datetime_created FROM users;

-- Update a post's content to "Hello to the people of the Earth!" where it's initial content is "Hello Earth!" by using the record's ID

UPDATE posts SET content  = "Hello to the people of the Earth" WHERE content = "Hello Earth";

-- Delete the user with an email of "johndoe@gmail.com"

DELETE FROM users WHERE email = "johndoe@gmail.com";